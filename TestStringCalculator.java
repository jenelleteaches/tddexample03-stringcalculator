import static org.junit.Assert.*;

import org.junit.Test;

public class TestStringCalculator {
	/* 
	Create a function that accepts a string and returns a value.
	Here are the rules:
		1-An empty string returns zero
		2-A single number returns the value
		3-Two numbers, comma delimited, returns the sum
		4-Two numbers, newline delimited, returns the sum
		5-Three numbers, delimited either way, returns the sum
		6-Negative numbers throw an exception
		7-Numbers greater than 1000 are ignored
	*/
	@Test
	public void testEmptyStringReturnsZero() {
		StringCalculator s = new StringCalculator();
		int result = s.calculate("");
		assertEquals(0, result);
	}
	
	@Test
	public void testSingleNumberReturnsValue() {
		StringCalculator s = new StringCalculator();
		int result = s.calculate("550");
		assertEquals(550, result);
	}

	// R3: Two numbers, comma delimited, returns the sum
	@Test
	public void testTwoNumbers() {
		StringCalculator s = new StringCalculator();
		int result = s.calculate("15,30");
		assertEquals(45, result);
	}
	
	// R4: Two numbers, newline delimited, returns the sum
	@Test
	public void testTwoNumbersWithNewLine() {
		StringCalculator s = new StringCalculator();
		int result = s.calculate("500\n300");
		assertEquals(800, result);
	}
	
}
